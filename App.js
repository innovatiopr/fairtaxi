import React, {Component} from "react";
import EntryPage from "./src/EntryPage";
import {StackNavigator} from "react-navigation";
import TripInput from "./src/TripInput";
import TripCalculationDetail from "./src/TripCalculationDetail";
import VirtualMeter from "./src/VirtualMeter";
import RatingTaxisDriver from "./src/RatingTaxisDriver";
import PlaceRecommendations from "./src/PlaceRecommendations";
import SplashScreen from "react-native-splash-screen"

export default class App extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount(){
        SplashScreen.hide();
    }

    render() {
        return (<MainNew/>);
    }


}


const FairCalculator = StackNavigator(
    {
        init: TripInput,

    },
    {
        initialRouteName: 'init',
        headerMode: 'none',
        cardStyle: {backgroundColor: '#F4F4F4',},
        navigationOptions: {
            gesturesEnabled: false,
        },
    }
);

const MainNew = StackNavigator(
    {
        fairCalculator: FairCalculator,
        entryPage: EntryPage,
        tripCalculationDetail: TripCalculationDetail,
        ratingTaxisDriver: RatingTaxisDriver,
        placeRecommendations: PlaceRecommendations,
        virtualMeter: VirtualMeter
    },
    {
        initialRouteName: 'entryPage',
        headerMode: 'none',
        navigationOptions: {
            gesturesEnabled: false,
        },
    }
);