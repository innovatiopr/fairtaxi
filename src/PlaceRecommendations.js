import React, {Component} from "react";
import {FlatList, Image, StyleSheet, Text, TouchableOpacity, View, Dimensions} from "react-native";
import * as APICalls from "./apis/APICalls";
import {TabView, TabBar, SceneMap} from "react-native-tab-view";
import Communication from 'react-native-communications';
import Icon from "./Component/Icon";
import * as appStyles from "./appStyle";
import NavArrowButton from "./Component/NavBackButton";

export default class PlaceRecommendations extends Component {

    constructor(props) {
        super(props);
        let routes = [];
        routes.push({
            index: 0,
            key: 'RESTAURANT',
            title: 'Restaurantes',
        });
        routes.push({
            index: 1,
            key: 'ATTRACTION',
            title: 'Atraciones',
        });
        const {navigation} = props;
        let type = navigation.getParam('type', 'RESTAURANT');

        this.state = {
            restaurants: [],
            attractions: [],
            tabView: {index: type === "RESTAURANT" ? 0 : 1, routes: routes}
        };
        this.renderTab = this.renderTab.bind(this);
        this.renderRecommendation = this.renderRecommendation.bind(this);
    }

    componentWillMount() {
        navigator
            .geolocation
            .getCurrentPosition(({coords}) => {
                this.setState({currentLocation: coords}, () => {
                    this.fetchRecommendations(0);
                    this.fetchRecommendations(1);
                });

            });
    }

    fetchRecommendations(route) {

        let {currentLocation} = this.state;
        let payload = {
            "type": route === 0 ? "RESTAURANT" : "ATTRACTION",
            "location": {
                "lat": currentLocation.latitude,
                "lng": currentLocation.longitude
            }
        };

        APICalls
            .fetchRecommendations(payload)
            .then(response => {
                console.log(response);
                let data = {};
                if (route === 0)
                    data = {restaurants: response.recommendations};
                if (route === 1)
                    data = {attractions: response.recommendations};


                this.setState(data);
            });
    }


    render() {
        return (<TabView
            navigationState={this.state.tabView}
            renderScene={(route) => this.renderScene(route)}
            renderTabBar={props =>
                <TabBar
                    {...props}
                    style={{backgroundColor: '#FACC59',paddingTop:30}}
                    indicatorStyle={{backgroundColor: '#fff'}}
                />
            }
            onIndexChange={index => {
                this.setState({tabView: {...this.state.tabView, index: index}})
            }}
            initialLayout={{width: Dimensions.get('window').width, height: 100}}
        />)


    }

    renderScene = ({route}) => {
        return this.renderTab(route.index);
    };

    renderTab(route) {
        return (<View style={{flex: 1, margin: 10}}>
            <FlatList data={route === 0 ? this.state.restaurants : this.state.attractions}
                      renderItem={({item}) => {
                          return (this.renderRecommendation(item));
                      }}
                      ItemSeparatorComponent={this.renderSeparator}
            />
        </View>);
    }

    renderRecommendation(item) {
        return (<TouchableOpacity style={{height: 100, flexDirection: 'row'}} onPress={() => {
            this._navigateToFairCalculator(item);
        }}>
            <View style={{flex: 3, margin: 10}}>
                {item.image
                    ? (<Image style={{flex: 3}} source={{uri: item.image}}/>)
                    : (<Icon style={{fontSize: 100}} family="EvilIcons" name="image"/>)
                }
            </View>
            <View style={{flex: 7}}>
                <View style={{height: 20}}>
                    <Text style={styles.placeTitleText} numberOfLine={1}>{item.name}</Text>
                </View>
                <View style={{height: 20}}>
                    <Text style={styles.placeDescriptionText} numberOfLine={1}>{item.description}</Text>
                </View>
                <View style={{height: 20}}>
                    <TouchableOpacity style={{flex: 1}} onPress={() => {
                        this._doCall(item.phone);
                    }}>
                        <Text style={styles.telephoneText}>{item.phone}</Text>
                    </TouchableOpacity>
                </View>
                <View style={{height: 20, alignItems: 'flex-end', justifyItems: 'flex-end'}}>
                    <Text>{item.category}</Text>
                </View>
            </View>
            <View
                style={{flex: .8, alignItems: 'center', justifyContent: 'center'}}>
                <Icon family="EvilIcons" name="chevron-right"/>
            </View>
        </TouchableOpacity>)
    }

    renderSeparator(sectionID, rowID, adjacentRowHighlighted) {
        return (<View key={rowID} style={{marginLeft: 20, height: .5, backgroundColor: '#000'}}/>);
    }

    _doCall(phone) {
        Communication.phonecall(phone, true);
    }

    _navigateToFairCalculator(place) {
        this.props.navigation.navigate('fairCalculator', {
            destinationName: place.name,
            coordinates: {latitude: place.location.lat, longitude: place.location.lng}
        });

    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f4f4f4',
    },
    placeTitleText: {
        fontFamily: 'HelveticaNeue',
        fontSize: 14,
        fontWeight: 'bold'
    },
    placeDescriptionText: {
        fontFamily: 'HelveticaNeue',
        fontSize: 12,
        fontWeight: '200'
    },
    telephoneText: {
        fontFamily: 'HelveticaNeue',
        fontSize: 14,
        fontWeight: '200',
        textDecorationLine: 'underline',
        color: 'blue'
    }
});