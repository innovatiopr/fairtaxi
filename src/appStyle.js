import {PixelRatio, Platform, StyleSheet, Dimensions} from "react-native";


const platform = Platform.OS;

export function isIphoneX() {
    let d = Dimensions.get('window');
    const {height, width} = d;

    return (Platform.OS === 'ios' && (height === 812 || width === 812));
}

export default {
    marginTop: isIphoneX() ? 35 : 10
}