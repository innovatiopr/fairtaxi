import React, {Component} from "react";
import {FlatList, StyleSheet, Text, TextInput, TouchableOpacity, View} from "react-native";
import Icon from "./Component/Icon";
import AppButton from "./Component/AppButton";
import * as APICalls from "./apis/APICalls";
import NavArrowButton from "./Component/NavBackButton";
import * as appStyles from "./appStyle";

const options = [
    'Wrong Fare',
    'Bad Service',
    'Bad Driver',
    'Poor Vehicle Conditions',
    'Accident'];

export default class RatingTaxisDriver extends Component {

    constructor(props) {
        super(props);
        this.state = {
            stars: 5,
            lastStarMode: false,
            starsMode: [false, false, false, false, false],
            plate: null,
            currentLocation: {},
            selectedIssue: -1
        };
        this.save = this.save.bind(this);
    }

    componentWillMount() {
        navigator
            .geolocation
            .getCurrentPosition(({coords}) => {
                this.setState({currentLocation: coords});
            });
    }

    toggleStar(star) {
        this.setState({stars: star});
    }

    onChangeIssue(issueIndex) {
        this.setState({selectedIssue: issueIndex})
    }


    save() {
        let {currentLocation, stars, comments, plate} = this.state;
        let payload = {
            "rating": stars,
            "issue": this.state.selectedIssue > 0 ? options[this.state.selectedIssue] : "",
            "text": comments,
            "email": "",
            "tag": plate,
            location: {
                "latitude": currentLocation.latitude,
                "longitude": currentLocation.longitude
            }
        };
        APICalls
            .saveRating(payload)
            .then((response) => {
                if (response.status === 200)
                    this.props.navigation.navigate('entryPage');
            });
    }

    render() {

        return (<View style={styles.container}>
            <View style={{paddingLeft: 10, marginTop: appStyles.default.marginTop}}>
                <NavArrowButton onPress={() => this.props.navigation.navigate('entryPage')}/>
            </View>
            <View style={{flex: 0.1, marginLeft: 20, marginRight: 20, marginTop: 10, alignItems: 'center'}}>
                <Text style={styles.reviewText}>Reviews</Text>
            </View>

            <View style={{flex: 0.15, marginLeft: 20, marginRight: 20}}>
                <Text style={styles.reviewDescription}>
                    Have anything good or bad to express. Here is where you can rate that
                    ride and get heard.
                </Text>
            </View>

            <View style={{
                flex: 0.1,
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginLeft: 20, marginRight: 20,
            }}>
                {
                    Array.from(Array(5).keys()).map(i => {
                            return (
                                <TouchableOpacity key={i} onPress={() => this.toggleStar(i)}>
                                    <Icon style={{color: i <= this.state.stars ? '#F3CC6C' : 'black'}}
                                          family="AntDesign" name={i <= this.state.stars ? "star" : "staro"}/>
                                </TouchableOpacity>
                            )
                        }
                    )
                }

            </View>

            <View style={{flex: 0.35, marginLeft: 20, marginRight: 20}}>
                {this.state.stars <= 2
                    ?
                    (<FlatList data={options}
                               extraData={this.state}
                               scrollEnabled={false}
                               renderItem={({item, index}) => {
                                   return (
                                       <TouchableOpacity key={index}
                                                         style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}
                                                         onPress={() => {
                                                             console.log(index);
                                                             this.onChangeIssue(index);
                                                         }}>
                                           <View style={{flex: 0.90}}>
                                               <Text style={styles.optionText}>{item}</Text>
                                           </View>
                                           {
                                               index === this.state.selectedIssue
                                                   ? (<View style={{flex: 0.10}}>
                                                       <Icon style={{color: '#8FD3F6', textAlign: 'right'}}
                                                             family="AntDesign"
                                                             name="check"/></View>)
                                                   : (null)
                                           }
                                       </TouchableOpacity>)
                               }}
                    />)
                    : (null)}
            </View>

            <View style={{height: 40, marginTop: 10, marginLeft: 20, marginRight: 20}}>
                <TextInput style={styles.taxiPlate} placeholder={'Taxi Plate'}
                           onChangeText={(text) => this.setState({plate: text})}
                />
            </View>

            <View style={{height: 20, marginLeft: 20, marginRight: 20}}>
                <Text style={styles.descriptionLabel}>Description</Text>
            </View>
            <View style={{flex: 0.15, marginLeft: 20, marginRight: 20}}>
                <TextInput style={styles.issueDescription} placeholder={'Description'}
                           onChangeText={(text) => this.setState({comments: text})}
                           multiline={true}/>
            </View>

            <View style={styles.saveButton}>
                <AppButton buttonLabel={"Guardar"} onPress={this.save} enable={true}/>
            </View>


        </View>);
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f4f4f4',
    },
    reviewText: {
        fontFamily: 'HelveticaNeue',
        fontSize: 40,
        fontWeight: '400'
    },
    reviewDescription: {
        fontFamily: 'HelveticaNeue',
        fontSize: 18,
        textAlign: 'justify',
        fontWeight: '200'
    },
    optionText: {
        fontSize: 24,
        fontFamily: 'HelveticaNeue',
        color: '#F3CC6C'
    },
    taxiPlate: {
        flex: 1,
        fontFamily: 'HelveticaNeue',
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: '#ccc'
    },
    descriptionLabel: {
        fontFamily: 'HelveticaNeue',
        fontSize: 14,
        color: '#000'
    },
    issueDescription: {
        flex: 1,
        backgroundColor: 'white',
        fontFamily: 'HelveticaNeue',
        borderWidth: 1,
        borderColor: '#ccc'

    },
    saveButton: {
        flex: 0.1,
        margin: 20
    }

});