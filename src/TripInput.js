import React, {Component} from "react";
import {StyleSheet, Text, TouchableOpacity, View} from "react-native";
import AppButton from "./Component/AppButton";
import Incrementer from "./Component/Incrementer";
import NavArrowButton from "./Component/NavBackButton";
import NavResetButton from "./Component/NavResetButton";
import * as APICalls from "./apis/APICalls";
import * as appStyles from "./appStyle";


export default class TripInput extends Component {

    constructor(props) {
        super(props);
        this.state = {
            passengers: 1,
            baggage: 0,
            currentLocation: {},
            serviceType: 'TAXI_TURISTICO'
        };
        this.calculateFare = this.calculateFare.bind(this);
        this.passengerChange = this.passengerChange.bind(this);
        this.baggageChange = this.baggageChange.bind(this);
    }

    componentWillMount() {

        navigator
            .geolocation
            .getCurrentPosition(({coords}) => {
                console.log("_____ request______");

                let vitinCoords = {lat: coords.latitude, lng: coords.longitude};
                console.log(vitinCoords);
                this.setState({currentLocation: coords});
                APICalls.fetchDefaultServiceType(vitinCoords)
                    .then(response => {
                        console.log("_____ payload______");
                        console.log(response);
                        // console.log(`${JSON.stringify(coords)} - ${response.serviceType}`);
                        this.setState({serviceType: response.serviceType});
                    })

            });
    }

    passengerChange(value) {
        this.setState({passengers: value});
    }

    baggageChange(value) {
        this.setState({baggage: value});

    }

    calculateFare() {
        const {navigation} = this.props;
        this.props.navigation.navigate('tripCalculationDetail', {
            originCoordinates: this.state.currentLocation,
            destinationCoordinates: navigation.getParam('coordinates', {}),
            destinationName: navigation.getParam('destinationName', ''),
            passengers: this.state.passengers,
            baggage: this.state.baggage,
            serviceType: this.state.serviceType
        });
    }

    render() {
        const {navigation} = this.props;
        const destinationName = navigation.getParam('destinationName', 'Without Destiny');


        return (<View style={{flex: 1, backgroundColor: '#f4f4f4'}}>
            <View style={{flex: .20}}>
                <View style={{
                    flex: .45,
                    alignItems: 'flex-end',
                    justifyContent: 'space-between',
                    flexDirection: 'row',
                    paddingLeft: 10,
                    paddingRight: 10,
                    marginTop: appStyles.default.marginTop
                }}>
                    <NavArrowButton onPress={() => this.props.navigation.navigate('entryPage')}/>
                    <NavResetButton onPress={() => this.props.navigation.navigate('entryPage')}/>
                </View>
                <View style={{flex: .60, justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={{fontSize: 32, color: '#FACC59', textDecorationLine: 'underline'}}
                          numberOfLines={1}>{destinationName}</Text>
                    <Text style={{fontSize: 14, fontFamily: 'kohinoor Telugu', color: '#454545'}}>Destination</Text>
                </View>

            </View>
            <View style={{flex: .80, backgroundColor: '#3A3939', paddingLeft: 20, paddingRight: 20}}>

                <View style={{
                    flex: .25,
                    marginTop: 30,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                    <Text style={styles.inputLabel}>Taxi Type</Text>
                    <View style={{flex: 1, flexDirection: 'row', marginTop: 20}}>
                        <TouchableOpacity
                            onPress={() => {
                                this._onServiceTypeChange('TAXI')
                            }}
                            style={{
                                flex: .5,
                                height: 30,
                                borderColor: '#FACC59',
                                borderWidth: 1,
                                backgroundColor: this.state.serviceType === 'TAXI' ? 'rgba(250, 204, 89, 1)' : 'transparent',
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}>
                            <Text
                                style={[styles.text, {color: this.state.serviceType === 'TAXI' ? '#3A3939' : '#FACC59'}]}>None
                                Tourist Taxi</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => {
                                this._onServiceTypeChange('TAXI_TURISTICO')
                            }}
                            style={{
                                flex: .5,
                                height: 30,
                                borderColor: '#FACC59',
                                borderWidth: 1,
                                backgroundColor: this.state.serviceType === 'TAXI_TURISTICO' ? 'rgba(250, 204, 89, 1)' : 'transparent',
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}>
                            <Text
                                style={[styles.text, {color: this.state.serviceType === 'TAXI_TURISTICO' ? '#3A3939' : '#FACC59'}]}>Tourist
                                Taxi</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{flex: .25, alignItems: 'center'}}>
                    <Text style={styles.inputLabel}>Passenger</Text>
                    <Incrementer initAmount={1} onValueChange={this.passengerChange}/>
                </View>
                <View style={{flex: .25, alignItems: 'center'}}>
                    <Text style={styles.inputLabel}>Baggage</Text>
                    <Incrementer onValueChange={this.baggageChange}/>
                </View>
                <View style={{flex: .25, justifyContent: 'center'}}>
                    <AppButton buttonLabel={"Get the Fair Fare"} onPress={this.calculateFare} enable={true}/>
                </View>


            </View>

        </View>);
    }

    _onServiceTypeChange(type) {
        console.log(type);
        this.setState({serviceType: type})
    }


}

const styles = StyleSheet.create({
    inputLabel: {
        fontSize: 32,
        color: '#FACC59',
        fontFamily: 'HelveticaNeue-Light',
    },
    counterText: {
        fontSize: 63,
        color: '#FACC59',
        fontFamily: 'HelveticaNeue-bold',
        marginLeft: 15,
        marginRight: 15
    },
    text: {
        fontSize: 16,
        backgroundColor: 'transparent',
        color: '#3A3939',
        fontFamily: 'HelveticaNeue-Light',
        textAlign: 'center'
    },
});