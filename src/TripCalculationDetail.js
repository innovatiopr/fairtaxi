import React, {Component} from "react";
import {Dimensions, FlatList, InteractionManager, StyleSheet, Text, View} from "react-native";
import MapView, {Marker, Polyline, Polygon, ProviderPropType} from "react-native-maps";
import * as APICalls from './apis/APICalls';
import * as PolyDecoder from '@mapbox/polyline';
import AppButton from "./Component/AppButton";
import NavArrowButton from "./Component/NavBackButton";
import NavResetButton from "./Component/NavResetButton";
import * as appStyles from "./appStyle";

const {width, height} = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const DEFAULT_PADDING = {top: 40, right: 40, bottom: 40, left: 40};
const ZONES = APICalls.fetchZones();

export default class TripCalculationDetail extends Component {

    constructor(props) {
        super(props);
        this.state = {
            routeCoordinates: [],
            originCoordinates: {},
            destinationCoordinates: {},
            zoneCoordinates: [],
            destinationName: null,
            fees: [],
            total: 0.00,
            tripFeeType: false
        };
        this.navigateVirtualMeter = this.navigateVirtualMeter.bind(this);
    }

    componentWillMount() {
        const {navigation} = this.props;
        let originCoordinates = navigation.getParam('originCoordinates');
        let destinationCoordinates = navigation.getParam('destinationCoordinates');
        let destinationName = navigation.getParam('destinationName');
        let baggage = navigation.getParam('baggage');
        let passengers = navigation.getParam('passengers');
        let serviceType = navigation.getParam('serviceType');


        this.state = {
            originCoordinates: originCoordinates,
            destinationCoordinates: destinationCoordinates,
            destinationName: destinationName,
            baggage: baggage,
            passengers: passengers,
            serviceType: serviceType
        };

        APICalls.fetchSetOfCoords({
            origin: [originCoordinates.latitude, originCoordinates.longitude],
            destination: [destinationCoordinates.latitude, destinationCoordinates.longitude]
        }).then((json) => {
            console.log(json);
            let array = PolyDecoder.decode(json.routes[0].overview_polyline.points);
            let leg = json.routes[0].legs[0];

            let info = {distance: leg.distance, duration: leg.duration};
            let routeCoordinates = array.map((point) => {
                return {
                    latitude: point[0],
                    longitude: point[1]
                }
            });

            console.log(ZONES);

            let zoneCoordinates = [];
            let zoneGeometry = ZONES.features;
            let alpha = 1;
            let fillAlpha = 0.5;
            let colors = ['rgba(247, 206, 125',
            'rgba(173, 247, 125',
            'rgba(247, 131, 125',
            'rgba(125, 247, 204',
            'rgba(125, 190, 247',
            'rgba(161, 125, 247',
            'rgba(232, 125, 247',
            'rgba(247, 125, 175'];




            zoneGeometry.map((z, i) => {
                console.log(`zone = ${i}`);
                let zCoords = z.geometry.coordinates[0][0].map((point, i) => {
                    return {
                        latitude: point[1],
                        longitude: point[0]
                    }
                });

                console.log(`zonecoords  = ${i} - ${zCoords}`);

                zoneCoordinates.push({
                    zoneDescription: z.properties.name,
                    coordinates: zCoords,
                    fillColor: `${colors[i]},${fillAlpha})`,
                    color: `${colors[i]},${alpha})`
                });
            });

            console.log(zoneCoordinates);


            this.setState({
                routeCoordinates: routeCoordinates,
                duration: info.distance.text,
                distanceInMiles: info.distance.value / 1609.344,
                durationInSeconds: info.duration.value,
                zoneCoordinates: zoneCoordinates
            }, () => {
                this._callApi();
                InteractionManager.runAfterInteractions(() => {
                    this.map.fitToCoordinates(routeCoordinates, {
                        animated: true,
                        edgePadding: DEFAULT_PADDING
                    });
                })
            });
        });
    }

    _callApi() {
        const {originCoordinates, destinationCoordinates, distanceInMiles, passengers, baggage, durationInSeconds, serviceType} = this.state;
        let payload = {
            "serviceType": serviceType,
            "origin": {
                "lat": originCoordinates.latitude,
                "lng": originCoordinates.longitude
            },
            "dest": {
                "lat": destinationCoordinates.latitude,
                "lng": destinationCoordinates.longitude
            },
            "distanceInMiles": distanceInMiles,
            "passengers": passengers,
            "baggage": baggage,
            "phoneCall": false,
            "totalTripTimeSeconds": durationInSeconds
        };

        APICalls
            .retrieveFare(payload)
            .then(response => {

                response.fees.push({name: 'Suggested Tip (15%)', fee: response.suggestedTip});
                this.setState({
                    fees: response.fees,
                    total: response.total.toFixed(2),
                    tripFeeType: response.tripFeeType,
                    suggestedTip: response.suggestedTip,
                    totalWithTip: response.totalWithTip.toFixed(2)
                })
            });
    }


    render() {
        const {originCoordinates, destinationCoordinates, destinationName} = this.state;
        return (<View style={styles.container}>

            <MapView
                ref={ref => {
                    this.map = ref;
                }}
                style={{flex: 0.5, width: Dimensions.get('window').width}}
                initialRegion={{
                    latitude: originCoordinates.latitude,
                    longitude: originCoordinates.longitude,
                    latitudeDelta: LATITUDE_DELTA,
                    longitudeDelta: LONGITUDE_DELTA,
                }}
                region={this.state.region}
            >
                <Marker
                    coordinate={originCoordinates}
                    title="Origin"
                    description="Origin"
                />
                <Marker
                    coordinate={destinationCoordinates}
                    title={destinationName}
                    description=""
                />

                {this.state.routeCoordinates && this.state.routeCoordinates.length > 0
                    ? (<Polyline coordinates={this.state.routeCoordinates} strokeWidth={3}
                                 strokeColor="rgba(0, 0, 0, 0.5)" zIndex={1000}/>)
                    : (null)}

                {this.state.zoneCoordinates && this.state.zoneCoordinates.length > 0
                    ? this.state.zoneCoordinates.map((zone, i) => {
                        console.log(zone.fillColor);
                        return (<Polygon coordinates={zone.coordinates}
                                          zIndex={1000}
                                          fillColor={zone.fillColor}
                                          strokeWidth={1}
                                          strokeColor={zone.color}
                                          />);
                    })
                    : (null)
                }

            </MapView>

            <View style={{
                marginTop: appStyles.default.marginTop,
                marginLeft: 10,
                position: 'absolute',
            }}>
                <NavArrowButton onPress={() => this.props.navigation.goBack()}/>
            </View>
            <View style={{
                marginTop: appStyles.default.marginTop,
                marginLeft: 10,
                right: 10,
                position: 'absolute',
            }}>
                <NavResetButton onPress={() => this.props.navigation.navigate('entryPage')}/>
            </View>
            <View style={styles.fareView}>
                <Text style={styles.fareText}>${this.state.total}</Text>
                <Text style={styles.fareEstimateText}>Estimate Fare</Text>
            </View>


            <View style={styles.breakdownView}>

                <View style={{height: 20, alignItems: 'flex-end'}}>
                    <Text style={styles.etaText}>{this.state.duration}</Text>
                </View>

                <View style={{height: 30, marginBottom: 10}}>
                    <Text style={styles.breakdownTitle}>Fare Breakdown</Text>
                </View>

                {this.state.fees && this.state.fees.length > 0
                    ? (<FlatList
                            style={{flex: 0.3}}
                            data={this.state.fees}
                            renderItem={({item, index}) => {
                                return (this._renderBreakdownDetail(item.name, item.fee, index));
                            }}
                            extraData={this.state}
                        />
                    )
                    : (null)}
                <Text style={[styles.fareText, {marginTop: 10, textAlign: 'right'}]}>${this.state.totalWithTip}</Text>
            </View>

            {this._renderVirtualMeter()}
        </View>);
    }

    _renderVirtualMeter() {
        if (this.state.tripFeeType)
            return (<View style={styles.virtualMeterButton}>
                <AppButton buttonLabel={"Virtual Meter"} onPress={this.navigateVirtualMeter} enable={true}/>
            </View>);
        return (null)
    }

    navigateVirtualMeter() {
        const {navigation} = this.props;
        let originCoordinates = navigation.getParam('originCoordinates');
        let baggage = navigation.getParam('baggage');
        let passengers = navigation.getParam('passengers');
        this.props.navigation.navigate('virtualMeter', {
            originCoordinates: originCoordinates,
            baggage: baggage,
            passengers: passengers

        })
    }


    _renderBreakdownDetail(title, value, key) {
        return (
            <View key={key} style={{flex: 1, flexDirection: 'row', marginLeft: 20}}>
                <View style={{flex: .80}}>
                    <Text style={styles.breakdownFeeName}>{title}</Text>
                </View>
                <View style={{flex: .20}}>
                    <Text style={styles.breakdownFeeCharge}>${value.toFixed(2)}</Text>
                </View>
            </View>);
    }
}

TripCalculationDetail.propTypes = {
    provider: ProviderPropType,
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    fareView: {
        ...StyleSheet.absoluteFillObject,
        height: 85,
        width: 150,
        backgroundColor: 'rgba(255,255,255, 0.8)',
        borderRadius: 5,
        marginTop: (Dimensions.get('window').height / 2) - 130,
        marginLeft: Dimensions.get('window').width - 170,
        alignItems: 'center',
        justifyContent: 'center'
    },
    fareText: {
        fontSize: 36,
        fontWeight: 'bold',
        fontFamily: 'HelveticaNeue'
    },
    fareEstimateText: {
        fontSize: 24,
        fontFamily: 'HelveticaNeue',
        fontWeight: '100'
    },
    etaText: {
        fontFamily: 'HelveticaNeue',
        fontSize: 14,
    },

    breakdownView: {
        flex: 0.45,
        marginTop: 5,
        backgroundColor: 'white',
        margin: 10
    },
    breakdownTitle: {
        fontFamily: 'HelveticaNeue',
        fontSize: 18,
    },
    breakdownFeeName: {
        fontFamily: 'HelveticaNeue',
        fontSize: 18,
        fontWeight: '100',
        textAlign: 'left'
    },
    breakdownFeeCharge: {
        fontFamily: 'HelveticaNeue',
        fontSize: 18,
        fontWeight: '100',
        textAlign: 'right'
    },
    virtualMeterButton: {
        height: 30,
        margin: 10
    }
});
