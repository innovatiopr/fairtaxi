import Zones from "./zones";
let blocker = 0;
let app;

const DEBUG = true;
const DISTANCE_MATRIX_API = 'https://maps.googleapis.com/maps/api/distancematrix/json';
const ROAD_API = 'https://roads.googleapis.com/v1/snapToRoads';
const DIRECTIONS_API = 'https://maps.googleapis.com/maps/api/directions/json';
const API_KEY = 'AIzaSyBh9wZ1Gct7wcLpoy3d1Q_jIMn5fqNqiFQ';
const FAIR_FARE_API = 'https://fairtaxi.trenurbanoapp.com';

export function calculateDistance(payloadRequest) {
    if (DEBUG) console.log(`${DISTANCE_MATRIX_API}?units=imperial&origins=${payloadRequest.origin}&destinations=${payloadRequest.destination}&key=${API_KEY}`);
    return _get(`${DISTANCE_MATRIX_API}?units=imperial&origins=${payloadRequest.origin}&destinations=${payloadRequest.destination}&key=${API_KEY}`)
}

export function fetchSetOfCoords(payloadRequest) {
    if (DEBUG) console.log(`${DIRECTIONS_API}?origin=${payloadRequest.origin}&destination=${payloadRequest.destination}&language=es&units=imperial&key=${API_KEY}`);
    return _get(`${DIRECTIONS_API}?origin=${payloadRequest.origin}&destination=${payloadRequest.destination}&key=${API_KEY}`)
}

export function fetchRoadSetOfCoords(payloadRequest) {
    if (DEBUG) console.log(`${ROAD_API}?path=${payloadRequest.origin}|${payloadRequest.destination}&interpolate=true&key=${API_KEY}`);
    return _get(`${ROAD_API}?path=${payloadRequest.origin}|${payloadRequest.destination}&interpolate=true&key=${API_KEY}`)
}

export function retrieveFare(payload) {
    if (DEBUG) console.log(`${FAIR_FARE_API}/fairtaxi/fare-estimate`);
    console.log(payload);
    return _post(`${FAIR_FARE_API}/fairtaxi/fare-estimate`, payload);
}

export function saveRating(payload) {
    if (DEBUG) console.log(`${FAIR_FARE_API}/fairtaxi/tripReview`);
    console.log(payload);
    return _post(`${FAIR_FARE_API}/fairtaxi/tripReview`, payload, false);
}

export function fetchRecommendations(payload){
    if (DEBUG) console.log(`${FAIR_FARE_API}/fairtaxi/recommendations`);
    return _post(`${FAIR_FARE_API}/fairtaxi/recommendations`, payload);
}

export function fetchDefaultServiceType(payload){
    if (DEBUG) console.log(`${FAIR_FARE_API}/fairtaxi/default-service-type`);
    return _post(`${FAIR_FARE_API}/fairtaxi/default-service-type`, payload);
}

export function fetchZones(){
    return Zones;
}

function _post(resource, body, manageException = true) {
    return _fetchInternal(`${resource}`,
        {
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            credentials: "same-origin",
            body: JSON.stringify(body)
        }).then(response => {
        return _handleResponse(response, manageException);
    });
}

function _get(resource, manageException = true) {
    return _fetchInternal(`${resource}`).then(response => {
        return _handleResponse(response, manageException);
    });
}

function _handleResponse(response, manageException = true) {
    // setTimeout(() => {
    //     blocker--;
    //     _updateUI();
    // }, loadingMinTimeout);

    let httpCode = response.status;
    let error = false;


    switch (httpCode) {
        case 403:
            if (manageException) {
                sessionStorage.setItem("previousSessionExpired", "1");
                window.location = "/";
                return;
            }
            break;
        case 400:
        case 404:
        case 500:
            if (manageException)
            // app.openModal("!!!Upps!!!", "Ocurrio un error");
                error = true;
            break;
        default:
            break;
    }
    if (error) {
        console.log(response.statusText);
        if (!manageException)
            return response;
    } else {
        if (manageException)
            return response.json();
        else
            return response;
    }
}

function _fetchInternal(url, options) {
    blocker++;
    _updateUI();
    if (!options)
        options = {};
    return fetch(url, options);
}

function _updateUI() {

    if (app) {
        app.forceUpdate();
    }
}