import React, {Component} from "react";
import {TouchableOpacity,StyleSheet,Text} from "react-native";
import PropTypes from "prop-types";

export default class AppButton extends Component {

    constructor(props) {
        super(props);
    }


    componentWillReceiveProps(nextProps, prevState) {
        this.setState({enable: nextProps.enable})
    }

    getDerivedStateFromProps(nextProps, prevState) {
        this.setState({enable: nextProps.enable})
    }

    render() {
        let activeOpacity = this.props.enable?.5:1;
        return (
            <TouchableOpacity
                {...this.props}
                activeOpacity={activeOpacity}
                style={[styles.base, this.props.enable ? styles.enable : styles.disabled,this.props.style]}>
                <Text style={styles.text}>{this.props.buttonLabel}</Text>
            </TouchableOpacity>
        );
    }
}

AppButton.propTypes = {
    buttonLabel: PropTypes.string,
    enable: PropTypes.bool
};


const styles = StyleSheet.create({
    base: {
        alignItems: 'center',
        height: 50,
        justifyContent: 'center',
        borderRadius: 5
    },
    enable: {
        backgroundColor: '#FACC59'/*Styles.color.green*/,
    },
    disabled: {
        // backgroundColor: '#fae297'/*Styles.color.green*/,
        backgroundColor: '#FACC59'/*Styles.color.green*/,
    },
    text:{
        fontSize:28,
        color: '#3A3939',
        fontFamily: 'HelveticaNeue-Light'
    }
});
