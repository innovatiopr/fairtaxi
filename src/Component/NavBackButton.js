import React, {Component} from "react";
import Icon from "./Icon";
import {TouchableOpacity} from "react-native";


export default class NavArrowButton extends Component {

    constructor(props) {
        super(props);
    }


    render() {
        return (
            <TouchableOpacity style={[{padding: 5}, this.props.style]} {...this.props}>
                <Icon family={"Feather"} name={"arrow-left"}  style={{color:'#F99E3C'}} />
            </TouchableOpacity>
        );
    }
}
