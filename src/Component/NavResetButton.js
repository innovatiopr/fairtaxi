import React, {Component} from "react";
import Icon from "./Icon";
import {TouchableOpacity} from "react-native";


export default class NavResetButton extends Component {

    constructor(props) {
        super(props);
    }


    render() {
        return (
            <TouchableOpacity style={[{padding: 5}, this.props.style]} {...this.props}>
                <Icon family={"AntDesign"} name={"close"}  style={{color:'#F99E3C',fontSize:30}} />
            </TouchableOpacity>
        );
    }
}
