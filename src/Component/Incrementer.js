import React, {Component} from "react";
import {Animated, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import Icon from "./Icon";
import PropTypes from "prop-types";


export default class Incrementer extends Component {

    constructor(props) {
        super(props);

        this.state = {
            counter: this.props.initAmount | 0,
            fadeAnim: new Animated.Value(1),
            fadeAnimInc: new Animated.Value(1)
        };

        this.renderAddButton = this.renderAddButton.bind(this);

    }

    _onValueChange(){
        this.props.onValueChange(this.state.counter);
    }

    _add() {
        this.setState((state, props) => ({
            counter: state.counter + 1
        }), ()=>{
            this._onValueChange();
        });
    }

    _substract() {
        if (this.state.counter === 0) return;
        this.setState((state, props) => ({
            counter: state.counter - 1
        }), ()=>{
            this._onValueChange();
        });
    }

    _addButtonClick() {
        Animated.timing(
            this.state.fadeAnim, // The value to drive
            {
                toValue: 0, // Animate to final value of 1
                duration: 100,
            }
        ).start(); // Start the animation
        Animated.timing(
            this.state.fadeAnimInc, // The value to drive
            {
                toValue: 1, // Animate to final value of 1
                duration: 100,
            }
        ).start(); // Start the animation
    }


    render() {
        return (
            <React.Fragment>
                {this.renderIncrementers()}
            </React.Fragment>);
    }


    renderIncrementers() {
        return (
            <Animated.View style={{flexDirection: 'row', alignItems: 'center',opacity: this.state.fadeAnimInc}}>
                <TouchableOpacity style={styles.ovalSmall} onPress={() => this._add()}>
                    <Icon family={"Feather"} name={"chevron-up"} style={{color: '#616161'}}/>
                </TouchableOpacity>
                <Text style={styles.counterText}>{this.state.counter}</Text>
                <TouchableOpacity style={styles.ovalSmall} onPress={() => this._substract()}>
                    <Icon family={"Feather"} name={"chevron-down"} style={{color: '#616161'}}/>
                </TouchableOpacity>
            </Animated.View>
        );
    }


    renderAddButton() {
        return (
            <Animated.View style={[styles.ovalLarge, {opacity: this.state.fadeAnim}]}>
                <TouchableOpacity
                    onPress={() => this._addButtonClick()}>
                    <Icon family={"Feather"} name={"plus"} style={{fontSize: 39, color: '#F99E3C'}}/>
                </TouchableOpacity>
            </Animated.View>

        );
    }


}

Incrementer.propTypes = {
    initAmount: PropTypes.number
};

const styles = StyleSheet.create({

    counterText: {
        fontSize: 63,
        color: '#FACC59',
        fontFamily: 'HelveticaNeue-bold',
        marginLeft: 15,
        marginRight: 15
    },
    ovalLarge: {
        borderRadius: 58 / 2,
        width: 58,
        height: 58,
        borderWidth: 6,
        borderColor: '#616161',
        alignItems: 'center',
        justifyContent: 'center'
    },
    ovalSmall: {
        borderRadius: 36 / 2,
        width: 36,
        height: 36,
        borderWidth: 2,
        borderColor: '#616161',
        alignItems: 'center',
        justifyContent: 'center'
    }
});
