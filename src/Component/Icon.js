import * as React from "React";
import {StyleSheet} from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import Entypo from "react-native-vector-icons/Entypo";
import Feather from "react-native-vector-icons/Feather";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Foundation from "react-native-vector-icons/Foundation";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import Octicons from "react-native-vector-icons/Octicons";
import Zocial from "react-native-vector-icons/Zocial";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import EvilIcons from "react-native-vector-icons/EvilIcons";
import AntDesign from "react-native-vector-icons/AntDesign";
import appStyle from "../appStyle";


export interface Props {
    family: string;
    name: string;
    style: any
}

export default class Icon extends React.Component<Props, State> {

    constructor(props) {
        super(props);
    }

    componentWillMount() {
        this.vectorIcon = Ionicons;
        switch (this.props.family) {
            case "Ionicons":
                this.vectorIcon = Ionicons;
                break;
            case "Entypo":
                this.vectorIcon = Entypo;
                break;
            case "Feather":
                this.vectorIcon = Feather;
                break;
            case "FontAwesome":
                this.vectorIcon = FontAwesome;
                break;
            case "Foundation":
                this.vectorIcon = Foundation;
                break;
            case "MaterialIcons":
                this.vectorIcon = MaterialIcons;
                break;
            case "MaterialCommunityIcons":
                this.vectorIcon = MaterialCommunityIcons;
                break;
            case "Octicons":
                this.vectorIcon = Octicons;
                break;
            case "Zocial":
                this.vectorIcon = Zocial;
                break;
            case "SimpleLineIcons":
                this.vectorIcon = SimpleLineIcons;
                break;
            case "EvilIcons":
                this.vectorIcon = EvilIcons;
                break;
            case "AntDesign":
                this.vectorIcon = AntDesign
        }

        this.alternateStyle = {};
        if(this.props.left)
            this.alternateStyle = {marginRight: 16};
        if(this.props.right)
            this.alternateStyle = {marginLeft: 16};



    }

    render() {
        this.style = StyleSheet.flatten([{fontSize: 30, color: appStyle.defaultIconColor}, this.alternateStyle, this.props.style]);
        let Icon = (this.vectorIcon);
        return (<Icon name={this.props.name} style={this.style}/>);
    }
}

