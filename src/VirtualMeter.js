import React, {Component} from "react";
import {StyleSheet, Text, View} from "react-native";
import MapView from "react-native-maps";
import BackgroundGeolocation from "react-native-background-geolocation";
import AppButton from "./Component/AppButton";
// import KeepAwake from 'react-native-keep-awake';
import * as api from "./apis/APICalls";
import NavArrowButton from "./Component/NavBackButton";
import * as appStyles from "./appStyle";

const LATITUDE_DELTA = 0.00922;
const LONGITUDE_DELTA = 0.00421;
export default class VirtualMeter extends Component {

    constructor(props) {
        super(props);

        this.state = {
            enabled: false,
            isMoving: false,
            motionActivity: {activity: 'unknown', confidence: 100},
            odometer: 0,
            // MapView
            markers: [],
            coordinates: [],
            showsUserLocation: false,
            showSettings: false,
            fare: 0
        };
    }

    componentDidMount() {
        // Step 1:  Listen to events:
        BackgroundGeolocation.on('location', this.onLocation.bind(this));

        // Step 2:  #configure:
        BackgroundGeolocation.configure({
            distanceFilter: 10,
            url: "" + this.state.username,
            params: {},
            autoSync: false,
            stopOnTerminate: false,
            stopAfterElapsedMinutes: 10,
            startOnBoot: true,
            foregroundService: true,
            debug: false,
            logLevel: BackgroundGeolocation.LOG_LEVEL_VERBOSE,
        }, (state) => {
            this.setState({
                enabled: state.enabled,
                isMoving: state.isMoving,
                showsUserLocation: state.enabled
            });
        });

        this.changeTrackingState(this.state.enabled);


    }

    componentWillReceiveProps(newProps, prevState) {
        /*    if (newProps.enabled !== this.props.enabled) {
                this.changeTrackingState(newProps.enabled);
            }*/
    }

    /**
     * @event location
     */
    onLocation(location) {
        console.log('[event] location: ', location);

        if (!location.sample) {
            this.addCoordinates(location);
            this.setState({
                odometer: (location.odometer / 100).toFixed(1)
            });
        }
        this.setCenter(location);
    }

    /**
     * @event motionchange
     */
    onMotionChange(event) {
        console.log('[event] motionchange: ', event.isMoving, event.location);
        if (!event.isMoving)
            this.addStop(event.location);
        else if (!this.state.isMoving) {
            this.setState({isMoving: true,})
        }
        let location = event.location;
    }

    /**
     * @event activitychange
     */
    onActivityChange(event) {
        console.log('[event] activitychange: ', event);
        this.setState({
            motionActivity: event
        });
    }

    /*
        addStop(location) {
            let marker = {
                key: location.uuid,
                title: location.timestamp,
                heading: location.coords.heading,
                coordinate: {
                    latitude: location.coords.latitude,
                    longitude: location.coords.longitude
                }
            };

            this.setState({
                isMoving: false,
                markers: [...this.state.markers, marker],
                coordinates: [...this.state.coordinates, {
                    latitude: location.coords.latitude,
                    longitude: location.coords.longitude
                }]
            });
        }*/


    async addCoordinates(location) {

        const {navigation} = this.props;
        let originCoordinates = navigation.getParam('originCoordinates');
        let baggage = navigation.getParam('baggage');
        let passengers = navigation.getParam('passengers');
        this.setState((state, props) => ({
            coordinates: [...state.coordinates, {
                latitude: location.coords.latitude,
                longitude: location.coords.longitude
            }]
        }), () => {

            let obj = {
                "serviceType": "TAXI_TURISTICO",
                "origin": {
                    "lat": originCoordinates.latitude,
                    "lng": originCoordinates.longitude
                },
                "dest": {
                    "lat": location.coords.latitude,
                    "lng": location.coords.longitude
                },
                "distanceInMiles": (location.odometer / 1609.344),
                "idleTimeSeconds": 0,
                "passengers": passengers,
                "baggage": baggage,
                "phoneCall": false
            };
            api.retrieveFare(obj).then((r) => {
                console.log(r);
                this.setState({fare: r.total})
            });

        });


        // this.props.onLocationChange(obj);
    }

    setCenter(location) {
        if (!this.refs.map) {
            return;
        }

        this.refs.map.animateToRegion({
            latitude: location.coords.latitude,
            longitude: location.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA
        });
    }


    toggleEnabled() {
        let enabled = !this.state.enabled;
        this.changeTrackingState(enabled);

    }

    changeTrackingState(enabled) {

        this.setState({
            enabled: enabled,
            isMoving: false,
            showsUserLocation: false,
            coordinates: [],
            markers: [],
        });

        if (enabled) {
            console.log("turning map on");

            BackgroundGeolocation.start((state) => {
                this.setState({showsUserLocation: true});
            });
            // this.changeKeepAwake(true);
        } else {
            console.log("turning map off");
            BackgroundGeolocation.stop();
            BackgroundGeolocation.resetOdometer();
            this.changeKeepAwake(false);
        }
    }

    changeKeepAwake = (shouldBeAwake) => {
        /*if (shouldBeAwake) {
            KeepAwake.activate();
        } else {
            KeepAwake.deactivate();
        }*/
    };

    _goBack() {
        if (this.state.enabled)
            this.changeTrackingState(false);
        this.props.navigation.goBack()
    }

    render() {
        return (
            <View style={{flex: 1, justifyContent: 'space-between'}}>

                <MapView
                    style={{
                        flex: 1,
                        ...StyleSheet.absoluteFillObject
                    }}
                    ref="map"
                    showsUserLocation={this.state.showsUserLocation}
                    followsUserLocation={true}
                    scrollEnabled={true}
                    showsMyLocationButton={false}
                    showsPointsOfInterest={false}
                    showsScale={false}
                    showsTraffic={false}
                    toolbarEnabled={false}>
                    {renderPolyline(this.state.coordinates)}
                    {renderMarkers(this.state.markers)}
                </MapView>
                <View  style={{flex:1}} >
                    <View style={{
                        marginTop: appStyles.default.marginTop,
                        marginLeft: 20
                    }}>
                        <NavArrowButton onPress={() => this._goBack()}/>
                    </View>
                    <View style={{
                        marginTop: 50,
                        backgroundColor: 'rgba(0,0,0,.5)',
                        borderRadius: 5,
                        paddingBottom: 20,
                        paddingTop: 20,
                        alignItems: 'center',
                        marginLeft: 20, marginRight: 20
                    }}>

                        <Text style={{
                            fontSize: 100,
                            fontFamily: 'HelveticaNeue-Bold',
                            color: '#FACC59'
                        }}>{formatCurrencyLabel(this.state.fare)}</Text>
                    </View>
                </View>
                <AppButton
                    style={{marginLeft: 20, marginRight: 20, marginBottom: 40}}
                    enable={true}
                    buttonLabel={this.state.enabled ? "Stop" : "Activate"}
                    onPress={() => this.toggleEnabled()}/>
            </View>
        );
    }
}


const renderMarkers = (markers) => {
    let rs = [];
    markers.map((marker) => {
        rs.push((
            <MapView.Marker
                key={marker.key}
                coordinate={marker.coordinate}
                anchor={{x: 0, y: 0.1}}
                title={marker.title}>
                <View style={[styles.markerIcon]}></View>
            </MapView.Marker>
        ));
    });
    return rs;
};


const formatCurrencyLabel = (currency = 0) => {
    currency = Number(currency).toFixed(2);
    currency = currency.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
    return "$" + currency;
};
const renderPolyline = (coordinates) => {
    return <MapView.Polyline
        key="polyline"
        coordinates={coordinates}
        geodesic={true}
        strokeColor='rgba(0,179,253, 0.6)'
        strokeWidth={6}
        zIndex={0}/>
};

const styles = StyleSheet.create({

    markerIcon: {
        borderWidth: 1,
        borderColor: '#000000',
        backgroundColor: 'rgba(0,179,253, 0.6)',
        width: 10,
        height: 10,
        borderRadius: 5
    }
});