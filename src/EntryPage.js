import React, {Component} from "react";
import {Dimensions, Image, ImageBackground, StyleSheet, Text, TextInput, TouchableOpacity, View} from "react-native";
import Icon from "./Component/Icon";
import Ghostwriter from "react-native-ghostwriter";
import RNGooglePlaces from "react-native-google-places";

const options = {
    sequences: [
        {string: "Where to?", duration: 2000},
        {string: "Airport", duration: 2000},
        {string: 'Old San Juan', duration: 2000},
        {string: 'Mall of San Juan', duration: 2000},
        {string: 'Plaza Las Americas', duration: 2000},
    ]
};

const GooglePlaceOptions = {country: 'PR'};
const AIRPORT_COORDS = {latitude: 18.436435, longitude: -66.012409};

export default class EntryPage extends Component {

    constructor(props) {
        super(props);
        this.onDestinationSelection = this.onDestinationSelection.bind(this);
        this.onFairCalculatorSelector = this.onFairCalculatorSelector.bind(this);
        this.navigateToRating = this.navigateToRating.bind(this);
    }

    componentDidMount() {
        const {height, width} = Dimensions.get('window');
        console.log(`w = ${width} - h = ${height}`)
    }

    onDestinationSelection() {
        RNGooglePlaces
            .openAutocompleteModal(GooglePlaceOptions)
            .then((place) => {
                this.props.navigation.navigate('fairCalculator', {
                    destinationName: place.name,
                    coordinates: {latitude: place.latitude, longitude: place.longitude}
                });

            });
    }

    onFairCalculatorSelector() {
        //todo
    }

    navigateToRating() {
        this.props.navigation.navigate('ratingTaxisDriver');
    }

    render() {
        return (<ImageBackground style={styles.container} source={require('./img/Rectangle.png')}>
            <View style={{flex: 1 / 3, alignItems: "center", justifyContent: 'flex-end'}}>
                <Text style={styles.title}>Fair Taxi</Text>
                <Text style={styles.fareCalculatorText}>Fare Calculator</Text>
            </View>

            <View style={{
                flex: 1 / 3, paddingLeft: 20,
                paddingRight: 20,
                justifyContent: 'center'
            }}>
                <Text style={styles.titleDescription} numberOfLines={1}>Select your destination to calculate the
                    fare</Text>
                <TouchableOpacity style={styles.destination} onPress={this.onDestinationSelection}>
                    <View style={styles.destinationGhostText}>
                        <Ghostwriter string="Where to?"
                                     options={options}/>
                    </View>
                </TouchableOpacity>
                <View style={styles.quickView}>
                    <View style={styles.quickTextView}>
                        <Text style={styles.quickText}>Quick Places: </Text>

                        {this._renderQuickPlaceIcon("ios-airplane", "Ionicons", () => {
                            this._navigateToAirport()
                        })}
                        {this._renderQuickPlaceIcon("silverware-fork", "MaterialCommunityIcons", () => {
                            this._navigateToRestaurant()
                        })}
                        {this._renderQuickPlaceIcon("location-on", "MaterialIcons", () => {
                            this._navigateToAttractions()
                        })}

                    </View>
                </View>
            </View>
            <TouchableOpacity style={styles.reportView} onPress={this.navigateToRating}>
                <Text style={styles.reportDescription}>Complaints or Comments? Report it here</Text>
            </TouchableOpacity>

            <Image source={require("./img/stripe.png")}
                   style={{ alignSelf: 'flex-end'}}/>

        </ImageBackground>);
    }

    _navigateToAirport() {
        this.props.navigation.navigate('fairCalculator', {
            destinationName: 'Luis Muñoz Marín International Airport (SJU)',
            coordinates: {latitude: AIRPORT_COORDS.latitude, longitude: AIRPORT_COORDS.longitude}
        });
    }

    _navigateToRestaurant() {
        this._navigateRecommendations('RESTAURANT');
    }

    _navigateToAttractions() {
        this._navigateRecommendations('ATTRACTION');
    }

    _navigateRecommendations(type) {
        this.props.navigation.navigate('placeRecommendations', {
            type: type
        });
    }

    _renderQuickPlaceIcon(iconName, iconFamily, onPressAction) {
        return (<TouchableOpacity onPress={() => onPressAction()}>
            <Icon style={styles.quickIcon} family={iconFamily} name={iconName}/>
        </TouchableOpacity>)
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignContent: 'center',


    },
    destination: {
        flexDirection: 'row',
        backgroundColor: '#3A3939',
        height: 50,
        borderRadius: 3
    },
    destinationGhostText: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start',
        paddingLeft: 10
    },
    destinationText: {
        fontSize: 32,
        fontWeight: "300",
        color: '#616161',
        fontFamily: 'HelveticaNeue-Light',
    },
    destinationViewIcon: {
        flex: 0.2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    titleView: {
        flex: 0.15,
        marginTop: 100
    },
    title: {
        color: '#fff',
        fontSize: 50,
        textAlign: 'center',
        fontFamily: 'Kohinoor Telugu',
    },
    titleDescriptionView: {
        marginTop: 50,
        alignItems: 'flex-start'
    },
    titleDescription: {
        color: '#fff',
        fontSize: 16,
        marginBottom: 10
    },
    fareCalculatorView: {
        flexDirection: 'row',
        marginTop: 25,
    },
    fareCalculatorTextView: {
        justifyContent: 'center',
        marginTop: 45

    },
    fareCalculatorText: {
        fontSize: 16,
        color: '#161616',
        textAlign: 'center',
    },
    rightIcon: {
        flex: 0.09,
        fontSize: 35,
        color: '#000',
        textAlign: 'center',
    },
    quickView: {
        flex: 0.3,
        flexDirection: 'row',
        justifyContent: "center",
        alignItems: 'center',
        marginTop: 5,
    },
    quickTextView: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    quickAirportViewIcon: {
        flex: 0.1,
    },
    quickIcon: {
        color: '#454545',
        fontSize: 18,
        marginLeft: 10,
        paddingTop: 5
    },
    quickText: {
        color: '#fff',
        fontSize: 16,

    },
    reportView: {
        flex: 1 / 3,
        justifyContent: 'center'

    },
    reportDescription: {
        color: '#fff',
        fontSize: 16,
        textAlign: 'center',
        textDecorationLine: 'underline'
    },
});